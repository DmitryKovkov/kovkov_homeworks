package com.company;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

    public static void main(String[] args) throws IOException {
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int num = 0, min = 15;
        while (0!=1) {
            num = Integer.parseInt(reader.readLine());
            if (num == -1) break;
            while (num!=0) {
                if (num%10<min) min=num%10;
                num = num / 10;
            }
        }
        System.out.println(min);

	// write your code here
    }
}
