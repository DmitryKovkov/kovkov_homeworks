package Transport;

import lombok.AllArgsConstructor;
import lombok.NoArgsConstructor;
@AllArgsConstructor
public class Bus extends Car {
    public Bus(String name, int yearOfProduction, int price, int weight, String color) {
        super(name, yearOfProduction, price, weight, color);
    }
}
