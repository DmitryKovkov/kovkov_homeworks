import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Arrays;

public class homework06 {
    public static int search (int[] mas, int x) {
        int y=-1;
        for (int i = 0; i<mas.length; i++)
                   if (mas[i] == x) {
                       y = i;
                       break;}
        return y;
    }

    public static void delnull (int[] mas) {
        for (int j = 0; j < mas.length-1; j++) {
            for (int i = 0; i < mas.length - 1; i++) {
                if (mas[i] == 0) {
                    int a = mas[i];
                    mas[i] = mas[i + 1];
                    mas[i + 1] = a;
                }
            }
        }
    }

    public static void main(String[] args) throws IOException {
        int[] mas = {4,0,0,23,0,0,0,0,4,6,0,2,0,1};
        BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
        int x = Integer.parseInt(reader.readLine());
        System.out.println(search(mas,x));
        delnull(mas);
        System.out.println(Arrays.toString(mas));
    }
}
